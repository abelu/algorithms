/*
 * Anton Beluzhenko
 * 15.01.2017
 * Percolation
 */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

  private WeightedQuickUnionUF sites;
  private boolean[] openSites;
  private int sitesSize = 0;
  
  /**
   * Percolation.
   *
   * @param n - the size of a field.
   */
  public Percolation(int n) {
    if (n < 1) {
      throw new IllegalArgumentException("invalid percolation size");
    }
    sites = new WeightedQuickUnionUF(n * n + 2 + 1);
    openSites = new boolean[n * n + 1];
    sitesSize = n;
  }
  
  private int xyTo1D(int row, int col, int size) {
    return (row - 1) * size + col;
  }

  private void checkBounds(int row, int col, int size) {
    if (row <= 0 || row > size) {
      throw new IllegalArgumentException("row index is out of bounds");
    }
    if (col <= 0 || col > size) {
      throw new IllegalArgumentException("col index is out of bounds");
    }
  }
  
  /**
   * Opens a site.
   *
   * @param row - the row number of a site.
   * @param col - the column number of a site.
   */
  public void open(int row, int col) {
  
    checkBounds(row, col, sitesSize);
    
    if (row == 1) {
      sites.union(sitesSize * sitesSize + 1, xyTo1D(row, col, sitesSize));
    } 
    if (row == sitesSize) {
      sites.union(sitesSize * sitesSize + 2, xyTo1D(row, col, sitesSize));
    }
    
    openSites[xyTo1D(row, col, sitesSize)] = true;
    
    for (int i = -1; i <= 1; i += 2) {      
      if (row + i <= sitesSize && row + i >= 1 && isOpen(row + i, col)) {
        sites.union(xyTo1D(row, col, sitesSize),
            xyTo1D(row + i, col, sitesSize));
      }
      if (col + i <= sitesSize && col + i >= 1 && isOpen(row, col + i)) {
        sites.union(xyTo1D(row, col, sitesSize),
            xyTo1D(row, col + i, sitesSize));
      }
    }
  }
  
  /**
   * Returns the site open status.
   *
   * @param row - the row number of a site.
   * @param col - the column number of a site.
   * @return site open status.
   */
  public boolean isOpen(int row, int col) {
    checkBounds(row, col, sitesSize);
    return openSites[xyTo1D(row, col, sitesSize)];
  }
  
  /**
   * Return the site full status.
   *
   * @param row - the row number of a site.
   * @param col - the column number of a site.
   */
  public boolean isFull(int row, int col) {
    checkBounds(row, col, sitesSize);
    return sites.connected(xyTo1D(row, col, sitesSize),
        sitesSize * sitesSize + 1);
  }

  /**
   * Returns the number of open sites.
   * 
   * @return number of open sites.
   */
  public int numberOfOpenSites() {
    int r = 0;
    
    for (int i = 0; i < openSites.length; i++) {
      if (openSites[i]) {
        r++;
      }
    }
    
    return r;
  }

  public boolean percolates() {
    return sites.connected(sitesSize * sitesSize + 1, sitesSize * sitesSize + 2);
  }

  public static void main(String[] args) {
  }

}
