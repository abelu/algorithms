import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/*
 * Anton Beluzhenko
 * 15.01.2017
 * Percolation Stats
 */

public class PercolationStats {

  private static final double CONFIDENCE = 1.96;
  private double mean;
  private double stddev;
  private double confidenceLo;
  private double confidenceHi;

  /**
   * PercolationStats.
   *
   * @param n - the size of a field.
   * @param trials - the number of trials.
   */
  public PercolationStats(int n, int trials) {

    if (n <= 0) {
      throw new java.lang.IllegalArgumentException("Invalid grid size");
    }
    
    if (trials <= 0) {
      throw new java.lang.IllegalArgumentException("Invalid number of trials");
    }
    
    
    double[] result = new double[trials];
    int length = n * n;
    int[][] nodesArray = new int[length][2];
    Percolation percolation;
    
    for (int t = 0; t < trials; t++) {
      percolation = new Percolation(n);
      
      for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
          nodesArray[i * n + j][0] = i + 1;
          nodesArray[i * n + j][1] = j + 1;
        }
      }
      
      int randomIndex;
      int[] buffer;
      for (int i = 0; i < length; i++) {
        randomIndex = i + StdRandom.uniform(length - i);
        percolation.open(nodesArray[randomIndex][0], nodesArray[randomIndex][1]);
        buffer = nodesArray[i];
        nodesArray[i] = nodesArray[randomIndex];
        nodesArray[randomIndex] = buffer;
        if (percolation.percolates()) {
          break;
        }
      }
      
      result[t] = (double) percolation.numberOfOpenSites() / length;
    }
    
    this.stddev = StdStats.stddev(result);
    this.mean = StdStats.mean(result);
    this.confidenceLo = this.mean
        - PercolationStats.CONFIDENCE * this.stddev / Math.sqrt(result.length);
    this.confidenceHi =  this.mean
        + PercolationStats.CONFIDENCE * this.stddev / Math.sqrt(result.length);
  }
  
  public double mean() {
    return this.mean;
  }
  
  public double stddev() {
    return this.stddev;
  }
  
  public double confidenceLo() {
    return this.confidenceLo;
  }
  
  public double confidenceHi() {
    return this.confidenceHi;
  }
  
  /**
   * PercolationStats.
   *
   * @param args - arguments.
   */
  public static void main(String[] args) {    
    int n = Integer.parseInt(args[0]);   
    int t = Integer.parseInt(args[1]);
    
    PercolationStats stats = new PercolationStats(n, t);
    
    StdOut.println(stats.mean());
    StdOut.println(stats.stddev());
    StdOut.println(stats.confidenceLo());
    StdOut.println(stats.confidenceHi());
  }
}
